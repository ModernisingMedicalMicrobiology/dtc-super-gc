#!/usr/bin/bash

# get the raw fastq files from the EBI ENA
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR256/008/ERR2560198/ERR2560198.fastq.gza
or 
wget ftp://ftp.sra.ebi.ac.uk/vol1/ERA141/ERA1416606/fastq/G7944_nano.fastq.gz

# There is too much data! lets subsample in a very basic way.
zcat G97687_nano.fastq.gz | head -n 1600000 | gzip > G97687_nano.40k.fa.gz
or
zcat G7944_nano.fastq.gz | head -n 3200000 | gzip > G7944_nano.3.2m.fastq.gz

# some quality control? Number of reads/bases etc

# Now assemble with wtgdb2
wtdbg2 -t 2 -i G97687_nano.40k.fa.gz -fo contigs -L 5000

# generate a consensus sequence from the graph
wtpoa-cns -t 2 -i contigs.ctg.lay -fo G97687.ctg.lay.fa

# let use blast to look for some resistance genes from the assembly
blastn -query G97687.ctg.lay.fa -db resistance_genes.fa > G97687.blast.txt

blastx -query ../assemblies/G7944_nano.20k.ctg.lay.racon.polished.fa -db dbs/resistance_prots.fa > /tmp/G7944.blast.blastx.txt


# lets see if we can clean that up a bit
minimap2 -ax map-ont assemblies/G97687_40k.ctg.lay.fa G97687_nano.40k.fa.gz > G97687_nano.40k.sam

racon G97687_nano.40k.fa.gz G97687_nano.40k.sam assemblies/G97687_40k.ctg.lay.fa > assemblies/G97687_40k.ctg.lay.racon.fa

# nanopolish
~/soft/nanopolish/nanopolish index -s sequencing_summary.txt -d f5s_extracted/ G7944_nano.20k.fastq.gz
minimap2 -ax map-ont assemblies/G7944_nano.20k.ctg.lay.racon.fa G7944_nano.20k.fastq.gz | samtools view -bS - | samtools sort -o G7944_nano.20k.sorted.bam
