#!/usr/bin/bash
python ~/soft/nanopolish/scripts/nanopolish_makerange.py assemblies/G7944_nano.20k.ctg.lay.racon.fa \
	| parallel --results nanopolish.results -P 2 \
	~/soft/nanopolish/nanopolish variants --consensus \
	-o polished.{1}.vcf \
	-w {1} \
	-r G7944_nano.20k.fastq.gz \
	-b G7944_nano.20k.sorted.bam \
	-g assemblies/G7944_nano.20k.ctg.lay.racon.fa -t 2 \
	--min-candidate-frequency 0.1

~/soft/nanopolish/nanopolish vcf2fasta -g assemblies/G7944_nano.20k.ctg.lay.racon.fa polished.*.vcf > assemblies/G7944_nano.20k.ctg.lay.racon.polished.fa
