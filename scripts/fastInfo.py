import sys
from Bio import SeqIO
import numpy as np
import gzip 
from argparse import ArgumentParser, SUPPRESS


def getLens(f,t='fasta'):
    '''return list of sequence lengths from a fasta (default) or fastq file'''
    lens=[]
    if f[-2:] == 'gz':
        seqF = gzip.open(f,'rt')
        t='fastq'
    else:
        seqF = open(f,'rt') 
        t='fasta'

    for seq in SeqIO.parse(seqF,t):
        lens.append(len(seq.seq))
    return lens

def getStats(lengths):
    '''return statistics for list of sequence lengths'''
    d={}
    all_len=sorted(lengths)
    n2=int(sum(lengths)/2)
    csum=np.cumsum(all_len)
    csumn2=min(csum[csum >= n2])
    ind=np.where(csum==csumn2)
    d['max']=all_len[-1]
    d['N']=len(all_len)
    d['mean']=sum(all_len)/len(all_len)
    d['bases']=sum(all_len)
    d['N50']=all_len[ind[0][0]]
    d['median']=np.median(all_len)
    return d

def run(opts):
    files=opts.fileIn
    lens=map(getLens,files)
    stats=map(getStats,lens)
    print('file','N','mean','median','bases','N50','max',sep='\t')
    for f,s in zip(files,stats):
        print(f,s['N'],s['mean'],s['median'],s['bases'],s['N50'],s['max'],sep='\t')


if __name__=="__main__":
    parser = ArgumentParser(description='preMap component for Classflow clinical metagenomics classification workflow')
    parser.add_argument('-f', '--fileIn', nargs='+', required=True,
                             help='sequence file input')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

