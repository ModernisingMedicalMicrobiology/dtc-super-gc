#!/usr/bin/env python
import sys
from Bio import SeqIO
from pymongo import MongoClient
import gzip
import os
from argparse import ArgumentParser
import pandas as pd
import subprocess

class ripTar:
    def __init__(self,opts):
        self.tars=opts.tars
        self.tarConts=opts.tarConts
        self.fastq=opts.fastq
        self.ip=opts.ip
        self.port=opts.port
        self.run_name=opts.run_name

    def getSeqIds(self):
        self.seqIds=[]
        for seq in SeqIO.parse(gzip.open(self.fastq,'rt'),'fastq'):
            self.seqIds.append(seq.id)

    def getTarDict(self):
        tars=os.listdir(self.tarConts)
        self.tarDict={}
        for tar in tars:
            tarF=tar.replace('.txt','')
            tf='{0}/{1}'.format(self.tarConts,tar)
            self.tarDict[tarF]=open(tf,'rt').read().split('\n')
        self.fileTar=dict( (v,k) for k in self.tarDict for v in self.tarDict[k] )

    def getSeqToFile(self):
        client = MongoClient(self.ip, self.port)
        db = client[self.run_name]
        collection = db.alba_summary
        hce = collection.find({'read_id': {'$in': self.seqIds }})
        self.read2file={}
        for h in hce:
            self.read2file[h['read_id']]=h['filename']

    def getTarFileDict(self):
        tars=os.listdir(self.tars)
        self.tarFileDict={}
        for tar in tars:
            tf='{0}/{1}'.format(self.tars,tar)
            self.tarFileDict[tar]=tf


    def getSeqFilesTars(self):
        df=pd.DataFrame(self.seqIds,columns=['read_ids'])
        df['file']=df['read_ids'].map(self.read2file)
        df['tar']=df['file'].map(self.fileTar)
        df['tarFile']=df['tar'].map(self.tarFileDict)
        df.to_csv('ReadstarFiles.csv')
        self.df=df

    def splitTars(self):
        self.utars=self.df['tar'].unique()
        for tar in self.utars:
            if tar == 'nan': continue
            df=self.df[self.df['tar']==tar]
            files=df['file']
            with open('outfiles/{0}.list'.format(tar),'wt') as outf:
                outf.write('\n'.join(files))

    def extractTar(self):
        for tar in self.utars:
            if tar == 'nan': continue
            ff='outfiles/{0}.list'.format(tar)
            tf='{1}/{0}'.format(tar,self.tars)
            subprocess.run(['tar','xzf',tf,'--files-from',ff],cwd='/home/nick/dtc/f5s_extracted/')



    def run(self):
        self.getTarFileDict()
        self.getSeqIds()
        self.getTarDict()
        self.getSeqToFile()
        self.getSeqFilesTars()
        self.splitTars()
        self.extractTar()

def getOpts(parser):
    parser.add_argument('-s', '--run_name', required=True,
                         help='Specify sample name as used in mongoDB.')
    parser.add_argument('-ip', '--ip', required=False, default='127.0.0.1',
                         help='IP address for mongoDB database')
    parser.add_argument('-p', '--port', required=False, default=27017,type=int,
                         help='port address for mongoDB database')
    parser.add_argument('-t', '--tars',required=True,
                             help='location of tar files')
    parser.add_argument('-tc', '--tarConts',required=True,
                             help='location of tar conents files')
    parser.add_argument('-f', '--fastq',required=True,
                             help='location of tar files')
    return parser


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='rip subset of fast5 reads from tar files')
    parser = getOpts(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    rt=ripTar(opts)
    rt.run()


