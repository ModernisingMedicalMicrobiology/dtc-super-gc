params.inCsv=file('files.csv')
glist = file(params.inCsv)
Channel
        .from( glist )
        .splitCsv()
        .map { row -> tuple(row[0], row[1]) }
        .view()
        .into{ ginputs1 }


process assemble {
     tag { sample }
     cpus 4

     input:
     set val(sample),val(nano) from ginputs1

     output:
     set val(sample),file('contigs.ctg.lay'),val(nano) into assembled

     script:
     """
     ~/soft/wtdbg2/wtdbg2 -t ${task.cpus} -i ${nano} -fo contigs -L 5000
     """
}

process consensus {
     tag { sample }
     cpus 4

     publishDir 'assemblies',  mode: 'copy'

     input:
     set val(sample),file('contigs.ctg.lay'),val(nano) from assembled

     output:
     set val(sample),file( "${sample}.ctg.lay.fa" ),val(nano) into conseqs,conseqs2,conseqs3

     script:
     """
     ~/soft/wtdbg2/wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay -fo ${sample}.ctg.lay.fa
     """
}

process blastn {
     tag { sample }

     publishDir 'blastns',  mode: 'copy'

     input:
     set val(sample),file( "${sample}.ctg.lay.fa" ) from conseqs3
     output:
     set val(sample),file( "${sample}.blast.txt" ) into blast


     script:
     """
     blastn -query ${sample}.ctg.lay.fa -db /home/nick/dtc/resistance_genes.fa > ${sample}.blast.txt

     """
}

process annotate {
     tag { sample }
     cpus 4

     publishDir 'annotation',  mode: 'copy'

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano) from conseqs

     output:
     set val(sample),file( "*_prokka" ) into annotations

     script:
     """
     $HOME/prokka/bin/prokka --outdir ${sample}_prokka --prefix ${sample} ctg.lay.fa
     """
}

process map {
     tag { sample }
     cpus 4

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano) from conseqs2

     output:
     set val(sample),file("ctg.lay.fa"),val(nano),file('out.sam') into mapped, mapped2 

     script:
     """
     minimap2 -t ${task.cpus} -ax map-ont  ctg.lay.fa ${nano} > out.sam
     """
}


process samtools {
     tag { sample }
     cpus 4

     publishDir 'remaps',  mode: 'copy'

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano),file('out.sam') from mapped2

     output:
     set val(sample),file( "${sample}.stats.txt" ),file( "*.png" ) into samtooled

     script:
     """
     samtools view -Sb out.sam -o out.bam
     samtools sort -o ${sample}.sorted.bam out.bam
     samtools index ${sample}.sorted.bam
     python3 /home/nick/dtc/plotBam.py ${sample}.sorted.bam > ${sample}.stats.txt
     """
}

process racon {
     tag { sample }
     cpus 4

     publishDir 'assemblies',  mode: 'copy'

     input:
     set val(sample),file( "ctg.lay.fa" ),val(nano),file('out.sam') from mapped

     output:
     set val(sample),file( "${sample}.ctg.lay.racon.fa" ) into racons

     script:
     """
     racon ${nano} out.sam ctg.lay.fa > ${sample}.ctg.lay.racon.fa
     """
}

process blastnRacon {
     tag { sample }

     publishDir 'blastns',  mode: 'copy'

     input:
     set val(sample),file( "${sample}.ctg.lay.racon.fa" ) from racons
     output:
     set val(sample),file( "${sample}.racon.blast.txt" ) into blastRacon


     script:
     """
     blastn -query ${sample}.ctg.lay.racon.fa -db /home/nick/dtc/resistance_genes.fa > ${sample}.racon.blast.txt

     """
}


