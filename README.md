# DTC-super-GC
This is a tutorial repository written by Nick Sanderson of the Modernising Medical Microbiology (MMM) group based at the JR hospital within Nuffield Dept. of Medicine, the University of Oxford. The aim is to reproduce some of the work we did whilst investigating the super-gonorrhoea case. This will showcase the ability to quickly assemble an entire genome using long reads. As well as the limitations of only using long reads.

## Background
A patient in Oxford presented with GC symptons, that was confirmed by routine lab diagnostics. Alarmingly the infection was resistant to both ceftriaxone and azithromycin antibiotics and failed treatment by ceftriaxone plus doxycycline and subsequently spectinomycin. The case was widely reported in the national media as "World's worst super-gonorrhoea".

BBC https://www.bbc.co.uk/news/health-43840505

Guardian https://www.theguardian.com/society/2018/mar/28/uk-man-super-strength-gonorrhoea

Our paper in Eurosurveillance https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6152157/

We determined the genetic mechanisms reponsible for the resistance by using a combination of high-quality short Illumina reads and long nanopore reads to generate a high quality genome assembly. Here we are going to replicate a nanopore-only componant of the investigation using a new faster and higher quality assembly tool, and see how far we get without the short read Illumina sequences.

## Getting started
We'll work in the /tmp directory so change directory to this and download this git repo. Then change directory again so the scripts and databases are available with the same relative paths.

```
cd /tmp
git clone https://gitlab.com/ModernisingMedicalMicrobiology/dtc-super-gc.git
cd dtc-super-gc
```

## optional software required
Not all the software we need in DTC is installed, so we are going to use singularity image I created. Singularity is like docker in that it is a container for software, but doesn't need root access. It provides an environment for some of the software packeges to run from.

The image is available on DTC computers, if you are not part of the DTP it can be downloaded from my dropbox, 
```
wget https://www.dropbox.com/s/3wijlf519mj1kca/dtc-super-gb-2018-11-12-dfc7661d4af5.img
```

You can enter the environment as such.
```
singularity shell /usr/local/bioinformatics/dtc-super-gb-2018-11-12-dfc7661d4af5.img
```
However, some of your previous tools may not work in this environment, such as artemis. So best to only use it for the software sections that are not installed.

## download the raw fastq files from the EBI ENA
Using wget to download raw sequencing files is a common practice in bioninformatics, here we are downloading from the European Bioinformatics Institute Nucleotide Archive (EBI-ENA). The url for the sequencing project is available here https://www.ebi.ac.uk/ena/data/view/PRJEB26560.

Run this command to get the fastq file:

```
wget ftp://ftp.sra.ebi.ac.uk/vol1/ERA141/ERA1416606/fastq/G7944_nano.fastq.gz
```

Now we have the fastq file, we can see what is in it. Use "head" and "zcat" to look at the first few lines of data.

```
head -n 4 G7944_nano.fastq.gz | zcat 
```

This should print out a few lines. The first line is the sequence identifer and description. The second line is the ATGC sequence. The third line is just a seperater, to the fourth line, that is the quality sequence (PHRED scale of logarithmic values of probability base is correct).


It should look something like this. Don't worry about the gzip error message at the end.
``` 
@d855c145-1db2-438b-aba6-eaadb36ed0d3 runid=66c6099ccab4893d4c31697b8d59e28b513f8160 sampleid=MDR_GC_G7944_20180327 read=6 ch=22 start_time=2018-03-27T15:19:17Z barcode=none
CCCAAAGTGTCATAGGTCTGCGAAGCCGTCAAAATAAAGATAATTTCCCGACGAAACCGTCTGACCGCCGAAGAAAACAATATACGCTTCGCACAAGGCGGGCGGCAGCGGAACTCGCCATTCAAAGAGTCGGCATACTGTGCGTGTTCGAACTGGCATGGCGTTCCGAATGCCGTCTGAACGCGTTTTTAACCTATTATAATCCCGCGTCTTTACTGTCAACTTCGCGGTTCGCAAACCTCCCGCGTTACCAAAGCTAGGATTCGATATGTCAAACCAAAGCCTTGGTCATCTTCTCGGGCGGTCAGGTTCGACCACCTGCCACAGTTCAGGCAATCCAGAGCCACTACGGGCGCGAAAACGTCAAGCCATTACCTTCCGATACGGGCAACGCCATTGCCGTCGAGCTGGAACGCGCAAGGTGGATCGCGCAGGATTTGGGCGTCAAACAAACCGTACTCGACTTGAGCCTGATGCGGCAGATTGCTGAAATGCCCTGATGGACGATACCGCGCCATCAGAACCGCCGACAACGGCGTTTCCAATACTAACTTGTGGACGGGCGTAACGCCCTTTTTCCTGCTTTACGCCGCCATTTTTGCCAAGGGCAGGGGTATCCGGCACATCATCGCGGGCGGTGGTACTGAAACCGATTTTTCGGGCTATCCCGACTGCGCGGCGTGTTCACGTCAAATCCATGAACGTTACCTTAATCTGGCGATGGACTACGTTTTCAAATCCACACCTTAACTGATGGCTGACCAAGGCGCCGCAAACGTGGGCGTTGGCGGACCGAAATAGGCGTGTTGGACTACATCCTGAACAGACCCACCTGCTACAAGGGTATCGTCAGCGGCTCGCATGGTCGCCAGGCTGTATCTGCGCGAACGCGAACTGGCGGAATGTCTGGAAAATTAAAAAGGTAGTCTGAACGCGCAAAGCCTAAGAATACGATAATGCCCAAACTCCATATGTTTTACCTCAGCGGCAATGTAGAAATGAAGTCAGATATCGAAGTGCACGACATCAATTTACCGTGTTGACGACTATCCGCCAGAAAGCCGTCCGCACTCACTCAAATGCGTGGTTCCGGCGATACGGACAAAAATCCACATCGACGGTTGGCAGGTTGTCGGAATGGGCGGACGGTTACGACATCGCCGTATCCAGAAACGCCCAAAACGAAATCGCCTGAAACCTTTACGCGCCTGTATTTTGCCAATGTCGGCTATCGCGCAGGGCAGCTTGCCGAAGGCACTTGCCGTTGGGGCTGTTCATGCGCCACGCCTGCCGAAGCCAAACAAAAGCCCTGCAAACCCTGGTGACCGACTATGTTCGGCAGCATAAAGACAACAGAAAGAGACGTGGACAACCTGCGCGCTCCGAGCACCATCAGCAGTTTTCCATATCCGCCTGACCCCGAATCCGCACGGCAAACCCACGCCAGAGCTTTCAGGGCTATTTGCCCATTTGGAAGCAGCGGATTAATGAACATAAAACGACACGTAAAGCCACCTCCGGGCGGCGGTTTTTCACTGAAAGGAATCGGGATGAAAATTACCAAGATGTTACCTTCGACTCCTCGCACCTTATGCTCGACGGGCATGACGGCAAATGCAAAACCTGCACGGGCATACCTACAACTCAGAAATCGCCCGTTTCAGACAGCATAATCAAAGGCAGACGAAAGGAACGGTATGGTGATGGACTTTACCGACTTGAAAGCCATTGTGAAACAACACATTACCGACCATTTCGATACGCCTTCATCTACCACGAAGCCCAATGGTACTTGAATCTCAATCATGCGCTCTTGGAGGCTGGAACATGAAAACCCTGCGCCTGCCCTGTGCACCTGCCGAAATATGGCGGTTGAAATGTACGGACGTTTGAAAAGCGGGGCTGAACGTGTCTTAGCGTCAAGGTGTGGGAAACGCCGACATCGTGCGCGGAGTATGAAGGGGTAGGAAAGAGTATCTTGAACGGTATCGATATAGTAAATTCAAATAGGAACATGCCCAACCGCGTCATTCCCGCGCAGGCGGGAATCAGATATGATTTATCAGGAATATTTAAAAGTACAGCAATTCCAAATCTCTGGTTCCCGCCTGCGCGAAATATTTGAATGAAGCGATCCTTATTTGAATTACCCACTACTTTCAGTTTTTCTCCTGTGCGGATTCTCCCGTTCTTCAGACAGCATTCCATACCGACACACCCATTAAAGGAACACCCATGAAACTCCTCTCCACTCATTTCTCGTCCTCCTCGTCATGCCAGACATTTCTACATCGCCTGGCTGGAAATGACGCAGATTCCCAGCGAAAAGCGTGGAAACGTTCAAGCTGCCTTATGAATTTATGGAACAAAAGCAAAGTGCAGACCTGTTCGGCAACCAAAGGGCTGTATAACGGCTTTCTCGGCATCGGGCTGGTGTGGTCGCGGTTTGCCGTGCCGGATCATGCCGTTTACGGCTACCAACGATTCCGTTTCTCGGTTTCGTATTGATTCGCCATGCGTGGGGCGCGTTCTCTTCCGGCAACAAAGAGGGCATACTCGTCAAACAAGGTTTGCCGCATTTTTTTTACCG
+
:9-/:535471&'*(*,,../*,1)/2104.:;978<31230-14(+('*),%%$$&(&',')'01,07113)068,81:0..'/+++0-,1-1.0*0/.3*/%&(&-,4,6,+31247.3.)4,*&'((')*+-+(+*(,&,+.2626((+,+-3/676<645:.+(,,-)/1./0/03:820/3985'&$('++,40118845&(+55+55-&'#$%&'(+1/0,3.:.7./016.&*),5/(1*-,,281:6%&'*0,+9,9+*-,-'48(/0/*35-',*'(/0/3-,.24346)*61781/*,*(3),--0((&$&(,$'%++.)%48228.5,*&(%/,%&$%)(*',*,+,6:31696,/,4:1-3&))(*-2+.102487,4*+'+.*-&'.6162//0/39:623')%('))*),69;9-,(+$.(%.:+1.4430/''(')4(7;18--+/.1**)**2050004()+))'*(*'&)*)+)-').6-,+01276-,---39**'*40-/&&())448;///3*,&,+).1'-%-&(&($)*)),--0:364498500%+*-(+,476/+)())/0,*-'(+'-2,/697,63644+856/+23-,.258-82-01168::93447/+$*)+*'(%)'-1+0115;;<0-7768686594+,+(()()(.-1289/.'**++492/3714)+:9374/13.15446314,++/87))%%')(-*'%(38).-%'',*1..-***,*-''"%&%$,)+,*'''%'.45718/695648,3/.658)%)'%&$(&),,.4<9;87012+*1*)/)-,**'--(%0,+',*++')+)++/16781&..**+-($%#&'),+%14&%%'%$'%(+,*+())+4203)%/,-23/3712+'''''*)-,-(0/;81)((&%).015792-.)).0*$'$%-(&)*&&%&((+))',/),1+/11--//5)7<;-3473*$('+))&2*'(&$)&()(%&(*2''(,,//+,2...0&(*.1.-.+2.8*$'%&&$+%((,0,,,'%%##%$%$++)%542)3+#&&)+($%)**/'&)-255;9/4(,)212403/533/7;9/,+*')*,2::00,3*1,,'775:543-'++45858+828..*4-/0,1223755/314/0**+51-..8-.;86)'./)%$$&')(+,'&$$%$#''*125/16*899/./730331'(&&'-/86-**%5876//,/-&'),*0+.*'+()%)(%)8/;:,-)'+2.(((''(./3223''.0,4++4959;:562-(.73()46::68//),.)*/:/'///0078807.-)..329/,03-)(%&,/(%(+,:79683323.0&)++./351*('20+)&&*/&))%&+4,)/4-0379:89;66+588.++7/,,+-+-+5/0971:,',*.,(('&#$'').)3.112142$+1+'*2.-+&&'((+*,5%-'1*+('')((,6;77222,,+,).*)0,%(,(214972'(&+*(/9/.6/1,))+(().0/1,::87238:./020326'),&+1,08431()*178)-.2-/./0,/.1233*4(243243414/)0930//3;:30)$$(),+30036562())()--6)(.-)*&)++))25/.)0(+'')$('-*,/4/294*4'3105-)(('*'*.,1278----445153+,23,)*)(.23+,3,9.*,01;5344,))%+-*,,*-.6.8;,-+*(,/*.,.723351024+0,.*)2,('(&%&*'(%$'*01135+0--&($'')//2722%*+-24:97.)(*+692-2,+.1+11..266)%'$(%'))(,('10,*00364*2072*+4,,6665:5822:2./0/,(5063*-003600116.4+*)--776''%))31..)12*+187342349::52310(*+,142+//0(++,/',-(')*+11.0./0/)&(*43-+*)%((*,,31:46<;4-051/-')&*0+--5)*-/0227.69**61508::-*++-*-(,,)-3574:3./3,5+1--.971*68/+/,/&57538913,35---55)/)8.91-09941%-%)+'&'+.)&&)(&')&)**+73170.2+++0-2+.2)$'''%%%%%%-.986,548622657591281)1+('37:8871214%..34.5-(*04529834.3:/,2638/)+(-531:8-10199430*,.05-%&.')58,.89658779:57487*+''(&%4702;0'))+/16/269392210.-(&*)1/1456663:*((+*.5.)(,)&+)'-**67<559'1/(-0,3./-/&/32-,.)(*,493),--2(*.1602))'**5:837110,-,3,+420022999:717-1:0/9931---/-1245884445::97;:819;*-4/5852/1.01/5254318-*(0+)$&*)112..+.(*).98329-.4:-,+.+-0.+00$&'%%(++1,.31/-&%&'*1476;70----40/.)'&,+,*:85554133096752173<5-2-*/13;<::85-''%$
gzip: stdin: unexpected end of file
```

## Subsampling the data
We actually have too much data. We need to subsample the sequences so our assembly will run quickly and be successful. There are some very nice and sophisticated ways to subsample long read data, such as filtlong (https://github.com/rrwick/Filtlong). But we're not going to use that and instead use a classic bioinformatics hack and take the first N reads.

We are going to use zcat and head again (note the switch of commands around the pipe). We are taking the first 80,000 lines of the file. This equates to 20,000 reads that we are piping into "gzip" to redirect into a new compressed file.

```
zcat G7944_nano.fastq.gz | head -n 80000 | gzip > G7944_nano.20k.fastq.gz
```

## Data quality and read lengths
Lets check to see how many bases of DNA we have. We also want to see what our reads lengths, longer the better for assemblies.

There is a script in the scripts/ folder called fastInfo.py. Run that on the subsampled fastq.gz file.

```
python3 scripts/fastInfo.py -f G7944_nano.20k.fastq.gz 
```

This should output a tab seperated table of information, on line per file.


| file                       |      N |        mean |  median |       bases |    N50 |     max |
| -------------------------- | ------ | ----------- | ------- | ----------- | ------ | ------- |
| 'G7944_nano.20k.fastq.gz'  | 20,000 | 10,290.115… | 4,359.5 | 205,802,304 | 24,372 | 170,688 |


Looks like we have over 200 megabases (mb) of DNA sequences to assemble. The Genome is approximately 2.4 mb in length, so we should have just under 100x coverage. Along with the decent read lengths, the assemble should work well.

## Assemble the reads
We are going to use a de novo assembler just recently released for long reads, written by Ruan Jue. It uses a fuzzy de bruijn graph to quickly and relatively accurately assemble reads. It is called wtdbg2 and more information can be found on the github page https://github.com/ruanjue/wtdbg2.

First we need to assemble the reads into contigs layouts and edge sequences. The options here are -t that means we use two processing threads, -i for the input data and -L that takes the longest length N reads and drops smaller sub reads. 

```
/soft/wtdbg2/wtdbg2 -t 2 -i G7944_nano.20k.fastq.gz -fo contigs -L 5000
```

## generate consensus

Now we generate the consensus sequence, again using two processing threads. The input is the outpt from the previous step.

```
/soft/wtdbg2/wtpoa-cns -t 2 -i contigs.ctg.lay.gz -fo G7944.ctg.lay.fa
```

Lets see how this how well this worked. We can use the same python3 script as before to check the number of contigs, max length etc.

```
python3 scripts/fastInfo.py -f G7944.ctg.lay.fa
```

| file             | N |    mean |   median |     bases |       N50 |       max |
| ---------------- | - | ------- | -------- | --------- | --------- | --------- |
| G7944.ctg.lay.fa | 4 | 560,065 | 45,348.5 | 2,240,260 | 2,138,364 | 2,138,364 |

OK, not bad, 4 contigs and a max of 2.138 mb is pretty good, looks like we have the chromosome, a plasmid and some errant contigs.

## Annotate the genome using Prokka
We are going to use a piece of software called prokka (paper https://academic.oup.com/bioinformatics/article/30/14/2068/2390517, software https://github.com/tseemann/prokka) to annotate the genome. This will look for open reading frames (CDS), translate and blast them against a few databases of proteins. It also annotates various RNA motifs (tRNA, rRNAs, ncRNAs) as well as signal peptides. It provides a number of useful output files that can be used, amongst other things, to visualise out assembly.

Run the following command to create annotations files.
```
/soft/prokka/bin/prokka --outdir G7944_prokka --prefix G7944 G7944.ctg.lay.fa
```

We can now visualie them using the Artemis genome browser. Run artemis in a different terminal and the .gff file in the prokka output folder. Ignore the warnings.
```
/usr/local/bioinformatics/artemis/art G7944_prokka/G7944.gff &
```

Notice how some of the genes are split onto different reading frames. This is due to indel errors in the assembly. But what about the genes of interest to use for gonorrhea infection. Have they been affected by assembly errors?

Here's a comparison of the gyrA gene between the hubrid assembly (top) and a nanopore only assembly (bottom). See the disruption.

![](images/prokka_hybrid_vs_wtgdb2.png)

## blast results
Lets use blast to check the assembly. I've prebuilt a blast database using some AMR genes of interest that I extracted from the *perfect* hybrid assembly. We can use blastn to get an alignment of these and check for missassemblies.

To run blast, use this command. It should output a text file with the results and alignments. Then use a program like "less" to view the text file, or open it with the system text editor. If you use less, press "q" to "quit" when you are done.

```
blastn -query G7944.ctg.lay.fa -db dbs/resistance_genes.fa > G7944.blast.txt
less G7944.blast.txt
```

Looks like there are a few errors in there. Especially around homopolymer repeats.

We can also use blastx and a different database of translated AMR genes to look at the quality of the alignment. Blastx translates the six read frames of the nucleotide sequences and blasts against a protein database. It has a different penalty scoring so codons are kept together to minimise missense alignments from indel errors.

```
blastx -query G7944.ctg.lay.fa -db dbs/resistance_prots.fa > G7944.blastx.txt
less G7944.blastx.txt
```

Here are the genes from the original paper, their mutations, mechanisms, and the antibiotics they affect. Can we use this information to get a result from the protein blast?

| Gene  | Variant                            | Mechanism                                                                          | Antimicrobials affected |
| ----- | ---------------------------------- | ---------------------------------------------------------------------------------- | ----------------------- |
| 23S   | rRNA A2059G, 4 copies              | Decreased macrolide binding to 50S ribosome                                        | AZM                     |
| penA  | FC428 mosaic penA - 100% identity  | Reduced β-lactam acylation of penicillin binding protein (PBP) 2 CRO,              | PEN                     |
| penB  | G120K, A121D                       | Reduced influx through PorB1b CRO, PEN,                                            | TET                     |
| mtrR  | G45D, Promoter deletion            | Over-expression of MtrCDE efflux pump resulting in increased efflux AZM, CRO, PEN, | TET                     |
| ponA  | L421P                              | Reduced β-lactam acylation of PBP1                                                 | PEN                     |
| tetM  | Gene presence                      | Prevents tetracycline binding to the 30S ribosome                                  | TET                     |
| rpsJ  | V57M                               | Reduced affinity of 30S ribosome for tetracycline                                  | TET                     |
| gyrA  | S91F, D95A                         | Reduced quinolone binding to DNA gyrase                                            | CIP                     |
| parC  | S87R                               | Reduced quinolone binding to topoisomerase IV                                      | CIP                     |


So by this potint we've assembled an entire bacterial genome, annotated it and looked to see if we can see any AMR genes. We can make out the AMR genes are present and may even be able to see the mutations, but its not 100% perfect, so can we be certain what we see if real? Its amazing that we can get 98% of the way there this fast. But the final two percent requires more time and effort.

## Improve assembly 
So we want to improve the assembly quality. We can use a tool called racon to clean it up a bit (read the paper here https://genome.cshlp.org/content/early/2017/01/18/gr.214270.116). This needs us to take the original raw fastq sequences and map them back to the consensus sequence from our assembly.

### map raw reads back to contigs
We are going to use minimap2 for that first step. Run this command to generate a sam file. It will also pipe (|) data to samtools so we can generate an aligned and sorted bam file that we'll use later on.
```
minimap2 -ax map-ont G7944.ctg.lay.fa G7944_nano.20k.fastq.gz | tee G7944.sam | samtools view -bS - | samtools sort -o G7944.sorted.bam 
```

### use Racon to clean up assembly
Next run the racon command with the subsampled raw fastq file, the sam alignment file, and our consensus sequence. This will generate a new consensus sequence called "G7944.ctg.lay.racon.fa"

```
racon G7944_nano.20k.fastq.gz  G7944.sam G7944.ctg.lay.fa > G7944.ctg.lay.racon.fa
```

We can use the commands from before to look at our consensus sequence. Both fastaInfo.py and blastn.

Has there been an improvement? Can we get any better with nanopolish?

# Nanopolish
Nanopolish isn't installed in the singularity image so use a different terminal for this.
We can also use nanopolish to improve nanopore assemblies(paper https://www.nature.com/articles/nmeth.3444, software https://github.com/jts/nanopolish). This software uses the raw 'squiggle' data from the fast5 files to "polish" the assembly as the squiggles contain more information than just the ATGC basecalls. It uses an algorithm that "iteratively proposes and evaluates modifications to the assembly", and the "evaluation step uses a hidden Markov model to calculate the probability of observing a sequence of current signals given an arbitrary nucleotide sequence"

The entire nanopolish process has a few steps and needs all the fast5 files, these are prohiitively large for this practical, so the commands are here as a demonstrations but we can skip to the good bit at the end (step 3).


Step (1) Firstly the fast5 files need to be indexd.
```
/usr/local/bioinformatics/nanopolish/nanopolish index -s data/sequencing_summary.txt -d fast5_files/ G7944_nano.20k.fastq.gz
```

Step (2) Perform nanopolish. This splits the genome into segments and runs nanopolish in parralel to save time and memory. It also needs the sorted bam files from a previous step.
```
samtools index G7944.sorted.bam
python /usr/local/bioinformatics/nanopolish/scripts/nanopolish_makerange.py G7944_nano.20k.ctg.lay.racon.fa \
        | parallel --results nanopolish.results -P 2 \
        /usr/local/bioinformatics/nanopolish/nanopolish variants --consensus \
        -o polished.{1}.vcf \
        -w {1} \
        -r G7944_nano.20k.fastq.gz \
        -b G7944.sorted.bam \
        -g G7944_nano.20k.ctg.lay.racon.fa -t 2 \
        --min-candidate-frequency 0.1
```

Step (3) Generate a new consensus sequence from the vcf files.
You may need to copy the index files from the data folder for this to work.
```
/usr/local/bioinformatics/nanopolish/nanopolish vcf2fasta -g G7944.ctg.lay.racon.fa data/polished.*.vcf > G7944.racon.polished.fa
```

Now repeat the blastn to see if this has improved our assembly...

# Unicycler
We can use short reads and Unicycler (paper https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005595, software https://github.com/rrwick/unicycler) to improve the assemby further. This is what we used in the investigation.

First download the raw data from ebi and subsample, again there is too much data. Subsampling reduces the run time of the 
```
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR256/009/ERR2560139/ERR2560139_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR256/009/ERR2560139/ERR2560139_2.fastq.gz
zcat ERR2560139_1.fastq.gz | head -n 3200000 | gzip > ERR2560139_1.3.2m.fastq.gz
zcat ERR2560139_2.fastq.gz | head -n 3200000 | gzip > ERR2560139_2.3.2m.fastq.gz
```

Next, run the assembly with three threads. This will take a while (like 5 hours or more, so go get lunch if you really want to run it), so you can read the unicycler paper and the github page that has some nice visualtions. 
```
unicycler -1 ERR2560139_1.3.2m.fastq.gz -2 ERR2560139_2.3.2m.fastq.gz -l G7944_nano.20k.fastq.gz -o unicycler_assembly -t 3
```


# Nanopolish methylation
## disclaimer
*We didn't do any methylation studies in the above investigation and I have never looked at methylation in my career. This part of the practical is a demonstration that the technology exists and can be applied. It may be that there are aspects of the Neisseria gonorrhoeae that are affetced by different methylation states, please feel free to look into that and report back anything of interest.*

## methylation

We can use the raw ONT reads to look for methylated bases using nanopolish. We didn't do this for with the GC samples, but it can be run with the data as an example.

The subset of fast5 reads can be downloaded from a dropbox I setup. This is just the same subsample that was extracted from the fastq file earlier, so if you used more reads, they won't be present.
```
wget https://www.dropbox.com/s/n50feck9r01o18e/f5s.tar.gz
tar -xzf f5s.tar.gz
```

Next call methylation using nanopolish, the subsampled fastq files, out aligned bam file and the assembly.
You may need to copy the index files from the data folder for this to work.
```
samtools index G7944.sorted.bam
mv data/G7944_nano.20k.fastq.gz.index* ./
/usr/local/bioinformatics/nanopolish/nanopolish call-methylation -t 3 -r G7944_nano.20k.fastq.gz -b G7944.sorted.bam -g G7944.ctg.lay.fa  > methylation_calls.tsv
/usr/local/bioinformatics/nanopolish/scripts/calculate_methylation_frequency.py -i methylation_calls.tsv > methylation_frequency.tsv
```
This produces a file with genome positions and the potentially methylated DNA motifs. It could be combined with the prokka annotation to identify methylated regions of the genome.
